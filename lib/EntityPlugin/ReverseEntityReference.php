<?php

/**
 * @file
 * Contains crumbs_reverse_er_Crumbs_ReverseEntityReference.
 */

/**
 * Reverse entity reference lookup for crumbs parent.
 */
class crumbs_reverse_er_EntityPlugin_ReverseEntityReference implements crumbs_EntityPlugin {

  /**
   * Field name that links entities together.
   *
   * @var string
   */
  protected $fieldKey;

  /**
   * Entity bundles with above field name that acts as candidate parents.
   *
   * @var array
   */
  protected $parentBundles;

  /**
   * Entity bundles that are refereed to.
   *
   * @var array
   */
  protected $targetBundles;

  /**
   * {@inheritdoc}
   */
  public function __construct($field_key, array $target_bundles, array $parent_bundles) {
    $this->fieldKey = $field_key;
    $this->targetBundles = $target_bundles;
    $this->parentBundles = $parent_bundles;
  }

  /**
   * {@inheritdoc}
   */
  public function describe($api, $entity_type, $keys) {
    if (!empty($this->targetBundles[$entity_type])) {
      foreach ($this->targetBundles[$entity_type] as $bundle_name) {
        $instance = field_info_instance($entity_type, $this->fieldKey, $bundle_name);
        $api->addRule($bundle_name, $keys[$bundle_name]);
        $api->descWithLabel($instance['label'], t('Field'), $bundle_name);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function entityFindCandidate($entity, $entity_type, $distinction_key) {
    list($entity_id, , $bundle_name) = entity_extract_ids($entity_type, $entity);
    if (array_search($bundle_name, $this->targetBundles[$entity_type])) {
      return;
    }
    $efq = new EntityFieldQuery();
    $parent_hit = $efq->entityCondition('entity_type', $entity_type)
      ->entityCondition('bundle', $this->parentBundles[$entity_type])
      ->fieldCondition($this->fieldKey, 'target_id', $entity_id)
      ->execute();
    if (empty($parent_hit[$entity_type])) {
      return;
    }
    $parent_id = key($parent_hit[$entity_type]);
    $parent = entity_load($entity_type, array($parent_id));
    $parent = reset($parent);

    $uri = entity_uri($entity_type, $parent);
    return $uri['path'];
  }

}
